/**
 * View Models used by Spring MVC REST controllers.
 */
package com.test.gateway.web.rest.vm;
